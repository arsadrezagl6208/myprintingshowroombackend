package com.springsecuritydemo.service;

import com.springsecuritydemo.entity.Reviews;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface ReviewsServices {
    public List<Reviews> findAll();
    public void add(Reviews reviews);

}
