package com.springsecuritydemo.service.impl;
import com.springsecuritydemo.entity.Reviews;
import com.springsecuritydemo.repo.ReviewRepo;
import com.springsecuritydemo.service.ReviewsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReviewsServicesImpl implements ReviewsServices {
    @Autowired
    private ReviewRepo reviewRepo;
    @Override
    public List<Reviews> findAll() {
        List<Reviews> allReviews = reviewRepo.findAll();
        List<Reviews> descReviews = new ArrayList<>();
        for(int i=allReviews.size()-1;i>=0;i--){
            descReviews.add(allReviews.get(i));
        }
        return descReviews;
    }

    @Override
    public void add(Reviews reviews) {
        reviews.setDate(LocalDate.now());
        reviews.setTime(LocalTime.now());
        reviews.setLocalDateTime(LocalDateTime.now());
        reviewRepo.save(reviews);
    }
}
