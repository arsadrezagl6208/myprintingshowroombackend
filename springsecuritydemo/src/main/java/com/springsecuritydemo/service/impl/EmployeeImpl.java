package com.springsecuritydemo.service.impl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import com.springsecuritydemo.entity.Employee;
import com.springsecuritydemo.model.JwtRequest;
import com.springsecuritydemo.model.ResetPssword;
import com.springsecuritydemo.repo.EmployeeRepo;
import com.springsecuritydemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeImpl implements EmployeeService {
    @Autowired
    private EmployeeRepo employeeRepo;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public Employee save1(Employee emp) {
        emp.setPassword(this.bCryptPasswordEncoder.encode(emp.getPassword()));
        return employeeRepo.save(emp);
    }

    @Transactional
    @Override
    public String deleteByEmail(String email) {
        Employee employee = employeeRepo.findByEmail(email);
        if(employee != null) {
            employeeRepo.deleteByEmail(email);
            return "Deleted Successfully !!";
        }
        throw new UsernameNotFoundException("Email not found !!");
    }

    @Override
    public Employee findByEmail(String email) {
        return employeeRepo.findByEmail(email);
    }

    @Override
    public List<Employee> findALl() {
        return employeeRepo.findAll();
    }

    @Override
    public Employee editByEmail(Employee emp, String email) {
        Employee employee = employeeRepo.findByEmail(email);
        if(emp.getCity()!=null) employee.setCity(emp.getCity());
        if(emp.getName()!=null)employee.setName(emp.getName());
        if(emp.getCompanyName()!=null)employee.setCompanyName(emp.getCompanyName());
        employeeRepo.save(employee);
        return employee;
    }
    @Override
    public Employee editByEmailByAdmin(Employee emp, String email) {
        Employee employee = employeeRepo.findByEmail(email);
        employee.setRole(emp.getRole());
        employeeRepo.save(employee);
        return employee;
    }
    @Override
    public String loginByEmail(JwtRequest loginRes) {
        return null;
    }

    @Override
    public String resetPassword(ResetPssword resetPssword, String email) {
        Employee employee = employeeRepo.findByEmail(email);
        if(employee != null) {
            if (this.bCryptPasswordEncoder.matches(resetPssword.getOld_password(),employee.getPassword())) {
                employee.setPassword(this.bCryptPasswordEncoder.encode(resetPssword.getPassword()));
                employeeRepo.save(employee);
                return "Successfully updated password !!";
            }
            else throw new UsernameNotFoundException("You enter wrong email !!");
        }
        else throw new UsernameNotFoundException("You enter wrong password!!");
    }

    @Override
    public List<Employee> findAll() {
        List<Employee> findAll = employeeRepo.findAll();
        return findAll;
    }

    @Override
    public ResponseEntity<Map<String, Object>> SortWithPaginationData(String title, int page, int size, String sort, String columnName) {
        try {
            List<Order> orders = new ArrayList<>();
            if(sort.toLowerCase().contains("desc")){
                orders.add(new Order(Sort.Direction.DESC,columnName));
            } else {
                orders.add(new Order(Sort.Direction.ASC,columnName));
            }
            List<Employee> tutorials = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
            Page<Employee> pageTuts;
            if (title == null)
                pageTuts = employeeRepo.findAll(pagingSort);
            else
                pageTuts = employeeRepo.findByEmailContaining(title, pagingSort);
            tutorials = pageTuts.getContent();
            if (tutorials.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            Map<String, Object> response = new HashMap<>();
            response.put("tutorials", tutorials);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
