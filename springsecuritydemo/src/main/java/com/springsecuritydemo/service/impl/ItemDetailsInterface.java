package com.springsecuritydemo.service.impl;
import com.springsecuritydemo.dto.GetAllImage;
import com.springsecuritydemo.entity.ItemDetails;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

public interface ItemDetailsInterface {
    public String uploadImage(MultipartFile file, ItemDetails itemDetails) throws IOException;
    public byte[] getImage(String id) throws IOException;

    List<GetAllImage> getAllImage() throws IOException;
}
