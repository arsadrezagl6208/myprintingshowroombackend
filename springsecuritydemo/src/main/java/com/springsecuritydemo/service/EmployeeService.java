package com.springsecuritydemo.service;
import com.springsecuritydemo.entity.Employee;
import com.springsecuritydemo.model.JwtRequest;
import com.springsecuritydemo.model.ResetPssword;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface EmployeeService {

    public Employee save1(Employee emp);
    public String deleteByEmail(String email);
    public Employee findByEmail(String email);
    public List<Employee> findALl();
    public Employee editByEmail(Employee emp,String email);
    public Employee editByEmailByAdmin(Employee emp, String email);
    public String loginByEmail(@RequestBody JwtRequest loginRes);

    String resetPassword(ResetPssword resetPssword, String email);

    List<Employee> findAll();
    ResponseEntity<Map<String, Object>> SortWithPaginationData(String title, int page, int size, String sort,String columnName);
}
