package com.springsecuritydemo.repo;
import com.springsecuritydemo.entity.Employee;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import java.util.List;

@EnableJpaRepositories
public interface PaginationImpl extends PagingAndSortingRepository<Employee,String> {
    Page<Employee> findByEmailContaining(String email, Pageable pageable);
//    List<Employee> findByTitleContaining(String title, Sort sort);
}
