package com.springsecuritydemo.repo;
import com.springsecuritydemo.entity.Reviews;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepo extends JpaRepository<Reviews,Integer> {
}
