package com.springsecuritydemo.repo;

import com.springsecuritydemo.entity.ItemDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ItemRepo extends JpaRepository<ItemDetails,Integer> {
    Optional<ItemDetails> findByName(String id);
}
