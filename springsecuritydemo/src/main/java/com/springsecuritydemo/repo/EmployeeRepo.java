package com.springsecuritydemo.repo;
import com.springsecuritydemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee,String> {
    Employee findByEmail(String email);
    void deleteByEmail(String email);
    Page<Employee> findByEmailContaining(String email, Pageable pageable);

}
