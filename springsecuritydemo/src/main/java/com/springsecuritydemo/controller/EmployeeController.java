package com.springsecuritydemo.controller;

import com.springsecuritydemo.config.CustomUserDetailsService;
import com.springsecuritydemo.dto.GetAllImage;
import com.springsecuritydemo.entity.Employee;
import com.springsecuritydemo.entity.ItemDetails;
import com.springsecuritydemo.entity.Reviews;
import com.springsecuritydemo.helper.JwtUtil;
import com.springsecuritydemo.model.JwtRequest;
import com.springsecuritydemo.model.JwtResponse;
import com.springsecuritydemo.model.ResetPssword;
import com.springsecuritydemo.repo.ReviewRepo;
import com.springsecuritydemo.service.EmployeeService;
import com.springsecuritydemo.service.ReviewsServices;
import com.springsecuritydemo.service.impl.ItemDetailsInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class EmployeeController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private ItemDetailsInterface itemDetailsInterface;
    @Autowired
    private ReviewsServices repo;
    @Autowired
    private CustomUserDetailsService customUserDetailsService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private EmployeeService employeeService;
    @PostMapping("/save")
    public Employee save(@RequestBody Employee employee){
        return employeeService.save1(employee);
    }

    @PostMapping("/token")
    public ResponseEntity<?> login(@RequestBody JwtRequest jwtRequest) throws Exception {
        System.out.println(jwtRequest);
        try {
            this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(),jwtRequest.getPassword()));
        }catch(UsernameNotFoundException e) {
            e.printStackTrace();
            throw new Exception("Arsad Bad Credentials");
        }catch(BadCredentialsException e) {
            e.printStackTrace();
            throw new Exception(" Raza Bad Credentials");
        }

        UserDetails userDetails=this.customUserDetailsService.loadUserByUsername(jwtRequest.getUsername() );
        String token=this.jwtUtil.generateToken(userDetails);
        System.out.println("JWT.."+token);
        return ResponseEntity.ok(new JwtResponse(token));
       // return employeeService.loginByEmail(jwtRequest);
    }

    @GetMapping("/hello")
    public String abc(HttpServletRequest request){
        String token = request.getHeader("Authorization");
        if (token != null && token.startsWith("Bearer ")) {
            token = token.substring(7);}
        String tok=this.jwtUtil.getUsernameFromToken(token);
        String role = this.jwtUtil.getRoleFromToken(token);
        return tok+" role "+role;
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request){
        String token = request.getHeader("Authorization");
        return token;
    }

    @PutMapping("/edit/{email}")
    public ResponseEntity<?> editProfile(HttpServletRequest request,@RequestBody Employee employee, @PathVariable("email") String email){
        Employee emp = employeeService.editByEmail(employee,email);
        return ResponseEntity.status(HttpStatus.OK).body(emp);
    }
    @PutMapping("/editby/{email}")
    public ResponseEntity<?> editByAdmin(HttpServletRequest request,@RequestBody Employee employee, @PathVariable("email") String email){
        Employee emp = employeeService.editByEmailByAdmin(employee,email);
        return ResponseEntity.status(HttpStatus.OK).body(emp);
    }

    @GetMapping("/findBy/{email}")
    public Employee getByEmail(HttpServletRequest request,@PathVariable("email") String email){
        Employee emp = employeeService.findByEmail(email);
        System.out.println(emp);
        return emp;
    }
    @GetMapping("/findByadmin/{email}")
    public Employee getByEmailAdmin(HttpServletRequest request,@PathVariable("email") String email){
        Employee emp = employeeService.findByEmail(email);
//        System.out.println(emp);
        return emp;
    }


    @PostMapping("/reset/{email}")
    public String resetPassword(HttpServletRequest request,@PathVariable("email") String email,@RequestBody ResetPssword resetPssword){
            return employeeService.resetPassword(resetPssword,email);
    }
    @GetMapping("/findall")
    public List<Employee> findAll(HttpServletRequest request){
        return employeeService.findAll();
    }

    @DeleteMapping("/delete/{email}")
    public String deleteByEmail(HttpServletRequest request,@PathVariable("email") String email){
        return employeeService.deleteByEmail(email);
    }

    @PostMapping(value = "/upload", consumes = "multipart/form-data")
    public ResponseEntity<?> uploadImage(@Validated ItemDetails itemDetails, @RequestParam("image") MultipartFile image) throws IOException{
        String fileupload =  itemDetailsInterface.uploadImage(image,itemDetails);
        return ResponseEntity.status(HttpStatus.OK).body(fileupload);
    }
    @GetMapping("/image/{id}")
    public ResponseEntity<?> getImage(@PathVariable("id") String id) throws IOException {
        byte[] imageData = itemDetailsInterface.getImage(id);
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.valueOf("image/png")).body(imageData);
    }

    @GetMapping("/allimage")
    public ResponseEntity<?> allImage() throws IOException {
        List<GetAllImage> imageData = itemDetailsInterface.getAllImage();
        return ResponseEntity.status(HttpStatus.OK).body(imageData);
    }
    @PostMapping("/review")
    public void reviews(HttpServletRequest request,@RequestBody Reviews reviews)  {
        repo.add(reviews);
    }
    @GetMapping("/review")
    public List<Reviews> findAllReview() throws IOException {
        return  repo.findAll();
    }
    @GetMapping("/tutorials")
    public ResponseEntity<Map<String, Object>> getAllTutorialsPage(
            @RequestParam(required = false) String title,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "ASC") String sort,
            @RequestParam(defaultValue = "name") String columnName) {
        return employeeService.SortWithPaginationData(title,page,size,sort, columnName);
    }
}
