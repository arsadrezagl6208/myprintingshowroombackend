package com.springsecuritydemo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
@CrossOrigin
public class NormalUserController {

    @GetMapping("/normal")
    public String normalUser(){
        return "Normal User URL";
    }
}
