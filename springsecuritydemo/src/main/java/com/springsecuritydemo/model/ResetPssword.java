package com.springsecuritydemo.model;

import lombok.Data;

@Data
public class ResetPssword {

    private String password;
    private String email;
    private String old_password;
}
