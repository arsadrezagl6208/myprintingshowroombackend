package com.springsecuritydemo.dto;

import com.springsecuritydemo.entity.ItemDetails;
import lombok.Data;

@Data
public class GetAllImage {
    private ItemDetails itemDetails;
    private byte[] image;
    private double price;
    private String desc;
    private String id;
}
