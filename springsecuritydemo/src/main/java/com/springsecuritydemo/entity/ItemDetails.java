package com.springsecuritydemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "itemdetails")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ItemDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JsonIgnore
    private String name;
    private long price;
    private String desc;
    @JsonIgnore
    private String type;
    @Lob
    @Column(name = "imagedata",length = 1000)
    @JsonIgnore
    private String filepath;
}
