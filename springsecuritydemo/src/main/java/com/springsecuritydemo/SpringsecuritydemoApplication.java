package com.springsecuritydemo;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringsecuritydemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringsecuritydemoApplication.class, args);
	}
	@Bean
	public ApplicationRunner applicationRunner(HikariDataSource dataSource) {
		return args -> {
			// add a shutdown hook to close the connection pool
			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				dataSource.close();
			}));
		};
	}
}
